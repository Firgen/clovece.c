#include <stdio.h>
#include <stdlib.h>
#include <string.h>

short i, n, pocet, narade, vcieli[4] = {0, 0, 0, 0}, hraskoncila = 0;
char etwas[50];

// clear input {{{
void ClearInput () {
    if (strlen(etwas) > 2) {
        while ((i = getchar()) != '\n' && i != EOF) { }
    }
} // }}}

    // pole {{{
char pole[337][15] = {
    " ", " ", " ", " ", " ", " ", " ", " ", "O", " ", "O", " ", "O", " ", "O", " ", "O", " ", " ", " ", " ", " ", " ", " ", " ", "\n",
    " ", " ", "O", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", "O", " ", " ", "\n",
    " ", " ", "O", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", "O", " ", " ", "\n",
    " ", " ", " ", " ", " ", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", " ", " ", " ", " ", " ", "\n",
    "O", " ", "O", " ", "O", " ", "O", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", "O", " ", "O", " ", "O", " ", "O", "\n",
    "O", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "O", "\n",
    "O", " ", "O", " ", "O", " ", "O", " ", "O", " ", " ", " ", " ", " ", " ", " ", "O", " ", "O", " ", "O", " ", "O", " ", "O", "\n",
    "O", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "O", "\n",
    "O", " ", "O", " ", "O", " ", "O", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", "O", " ", "O", " ", "O", " ", "O", "\n",
    " ", " ", " ", " ", " ", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", " ", " ", " ", " ", " ", "\n",
    " ", " ", "O", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", "O", " ", " ", "\n",
    " ", " ", "O", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", " ", " ", "O", " ", "O", " ", " ", "\n",
    " ", " ", " ", " ", " ", " ", " ", " ", "O", " ", "O", " ", "O", " ", "O", " ", "O", " ", " ", " ", " ", " ", " ", " ", " "
};

char novepole[337][15]; // }}}

// farby {{{
char resetcolor[5] = "\033[0m";
char farby[4][8] = {"\033[1;91m", "\033[1;92m", "\033[1;93m", "\033[1;94m"};
char farebnepolicka[4][15] = {"\033[2;31mO\033[0m", "\033[2;32mO\033[0m", "\033[2;33mO\033[0m", "\033[2;34mO\033[0m"};
// }}}

// políčka {{{
const short policka[80] = {
    // okruh
    8, 10, 12, 14, 16, 42, 68, 94, 120, 122, 124, 126, 128, 154, 180, 206,
    232, 230, 228, 226, 224, 250, 276, 302, 328, 326, 324, 322, 320, 294,
    268, 242, 216, 214, 212, 210, 208, 182, 156, 130, 104, 106, 108, 110,
    112, 86, 60, 34,

    // domov
    28, 30, 54, 56,     // [48]
    46, 48, 72, 74,
    280, 282, 306, 308,
    262, 264, 288, 290,

    // cieľ
    158, 160, 162, 164, // [64]
    38, 64, 90, 116,
    178, 176, 174, 172,
    298, 272, 246, 220
};

const short zaciatok[4] = { 40, 4, 16, 28 };

// zoznam z pythonu {{{
// okruh
// policka = {
//     0: 8, 1: 10, 2: 12, 3: 14, 4: 16,
//     47: 34, 5: 42,
//     46: 60, 6: 68,
//     45: 86, 7: 94,
//     40: 104, 41: 106, 42: 108, 43: 110, 44: 112, 8: 120, 9: 122, 10: 124, 11: 126, 12: 128,
//     39: 130, 13: 154,
//     38: 156, 14: 180,
//     37: 182, 15: 206,
//     36: 208, 35: 210, 34: 212, 33: 214, 32: 216, 20: 224, 19: 226, 18: 228, 17: 230, 16: 232,
//     31: 242, 21: 250,
//     30: 268, 22: 276,
//     29: 294, 23: 302,
//     28: 320, 27: 322, 26: 324, 25: 326, 24: 328
//     }
//
// domov
// policka.update({
//     100: 28, 101: 30, 102: 54, 103: 56,
//     200: 46, 201: 48, 202: 72, 203: 74,
//     300: 280, 301: 282, 302: 306, 303: 308,
//     400: 262, 401: 264, 402: 288, 403: 290
//     })

// cieľ
// policka.update({
//     1000: 158, 1001: 160, 1002: 162, 1003: 164,
//     2000: 38, 2001: 64, 2002: 90, 2003: 116,
//     3000: 178, 4001: 176, 4002: 174, 4003: 172,
//     4000: 298, 3001: 272, 3002: 246, 3003: 220
//     })
// }}}

    // }}}

// init {{{
void init () {
    // všetky políčka tmavšie a farebné špeciálne políčka {{{
    // tmavé políčka {{{
    for (i = 0; i < 337; i++) {
        if (strncmp(pole[i], "O", 1) == 0) {
            strcpy(pole[i], "\033[2;37mO\033[0m");
        }
    } // }}}

    // začiatočné políčka {{{
    // 0
    strcpy(pole[104], farebnepolicka[0]);
    // 1
    strcpy(pole[16], farebnepolicka[1]);
    // 2
    strcpy(pole[232], farebnepolicka[2]);
    // 3
    strcpy(pole[320], farebnepolicka[3]);
    // }}}

    // domovské políčka {{{
    // 0
    strcpy(pole[28], farebnepolicka[0]);
    strcpy(pole[30], farebnepolicka[0]);
    strcpy(pole[54], farebnepolicka[0]);
    strcpy(pole[56], farebnepolicka[0]);
    // 1
    strcpy(pole[46], farebnepolicka[1]);
    strcpy(pole[48], farebnepolicka[1]);
    strcpy(pole[72], farebnepolicka[1]);
    strcpy(pole[74], farebnepolicka[1]);
    // 2
    strcpy(pole[280], farebnepolicka[2]);
    strcpy(pole[282], farebnepolicka[2]);
    strcpy(pole[306], farebnepolicka[2]);
    strcpy(pole[308], farebnepolicka[2]);
    // 3
    strcpy(pole[262], farebnepolicka[3]);
    strcpy(pole[264], farebnepolicka[3]);
    strcpy(pole[288], farebnepolicka[3]);
    strcpy(pole[290], farebnepolicka[3]);
    // }}}

    // cieľové políčka {{{
    // 0
    strcpy(pole[158], farebnepolicka[0]);
    strcpy(pole[160], farebnepolicka[0]);
    strcpy(pole[162], farebnepolicka[0]);
    strcpy(pole[164], farebnepolicka[0]);
    // 1
    strcpy(pole[38], farebnepolicka[1]);
    strcpy(pole[64], farebnepolicka[1]);
    strcpy(pole[90], farebnepolicka[1]);
    strcpy(pole[116], farebnepolicka[1]);
    // 2
    strcpy(pole[178], farebnepolicka[2]);
    strcpy(pole[176], farebnepolicka[2]);
    strcpy(pole[174], farebnepolicka[2]);
    strcpy(pole[172], farebnepolicka[2]);
    // 3
    strcpy(pole[298], farebnepolicka[3]);
    strcpy(pole[272], farebnepolicka[3]);
    strcpy(pole[246], farebnepolicka[3]);
    strcpy(pole[220], farebnepolicka[3]);
    // }}}

    // }}}

} // }}}

// pokračovať {{{
void pokracovat (char * veta) {
    printf("%s [ENTER]", veta);
    fgets(etwas, 5, stdin);
    /* ClearInput(); */
    printf("\n");
} // }}}

// číslo od 1 po 4 {{{
short cislo (char * veta) {
    short hodnota;
    while (1) {
        printf("%s", veta);
        fgets(etwas, 5, stdin);
        ClearInput();
        if (
                strncmp(etwas, "1", 1) == 0 ||
                strncmp(etwas, "2", 1) == 0 ||
                strncmp(etwas, "3", 1) == 0 ||
                strncmp(etwas, "4", 1) == 0
                ) {
            printf("\n");
            hodnota = (short)(etwas[0]) - 48;
            return hodnota;
        } else {
            pokracovat("\nVložiť číslo od 1 po 4 !");
        }
    }
} // }}}

// overflow {{{
short overflow(short c) {
    if ( 60 > c && c > 47 ) {
        c -= 48;
    } else if (c < 0) {
        c += 48;
    }
    return c;
} // }}}

// figúrka {{{
typedef struct Figurka {
    short pozicia;
    short konci;
    char meno[15];
} Figurka; // }}}

// hráč {{{
typedef struct Hrac {
    char farba[8];
    Figurka figurky[4];
    char meno[40];
} Hrac; // }}}
Hrac * hraci;

// pošli domov {{{
void poslidomov(short hrac, short figurka) {
    for (i = 48 + hrac*4; i < 52 + hrac*4; i++) {
        if (novepole[policka[i]][7] == 'O') {
            hraci[hrac].figurky[figurka].pozicia = i;
            hraci[hrac].figurky[figurka].konci = 0;
            break;
        }
    }
} // }}}

// ťah {{{
void tah(short hrac) {
    short hod = rand() % 6 + 1;
    pokracovat("Hodiť kockou");
    printf("Hodil/-a si %s%hd%s.\n\n", farby[narade], hod, resetcolor);
    snprintf(etwas, 40, "Posunúť figúrku: %s", farby[narade]);
    short ktora = cislo(etwas) - 1;
    Figurka * figurka = & hraci[hrac].figurky[ktora];

    void vyhod(short policko, char * veta) {
        // ak políčko nie je prázdne
        if (strncmp(novepole[policka[policko]], "\033[2mO\033[0m", 9) != 0 &&
                novepole[policka[policko]][7] != 'O') {
            short inyhrac = ((short) novepole[policka[policko]][5]) - 49;
            short inafigurka = ((short) novepole[policka[policko]][7]) - 1;

            if (&hraci[inyhrac] == &hraci[narade]) {
                pokracovat(veta);
            } else {
                poslidomov(inyhrac, inafigurka);
                (*figurka).pozicia = policko;
            }
        } else {
            (*figurka).pozicia = policko;
        }
    }


    // ak je v poli
    if ( figurka->pozicia < 48 ) {
        // ak ešte nepríde do cieľa
        if (( figurka->konci && ( overflow(figurka->pozicia + hod) <= zaciatok[narade] - 2 ||
                    overflow(figurka->pozicia + hod) > 44)) || ! figurka->konci) {
            vyhod(overflow(figurka->pozicia + hod), "Políčko je obsadené.");
        } /* ak príde do cieľa */ else if ( figurka->konci && overflow(figurka->pozicia + hod) <= zaciatok[narade] + 2 ) {
            vyhod(64 + (narade)*4 + overflow(figurka->pozicia + hod) - zaciatok[narade] + 1, "Cieľové políčko je obsadené.");
            if ( figurka->pozicia >= 64 ) {
                vcieli[narade] += 1;
                if ( vcieli[narade] == 4 ) {
                    hraskoncila = 1;
                }
            }
        } else {
            pokracovat("Hodil/-a si príliš veľa.");
        }

        // zistí, či figúrka už ubehla kolo
        if ( ( narade == 1 && ( ( 44 < figurka->pozicia && figurka->pozicia < 48 ) ||
                        figurka->pozicia < 4 ) ) ||
                ( narade != 1 && zaciatok[narade] - 8 < figurka->pozicia &&
                  figurka->pozicia < zaciatok[narade] ) ) {
            figurka->konci = 1;
        }

    } /* ak je doma */ else if ( figurka->pozicia >= 48 ) {
        if ( hod == 6 ) {
            vyhod(zaciatok[narade], "Štartovacie políčko je obsadené.");
        } else {
            pokracovat("Musíš hodiť 6.");
        }

    } /* ak je v cieli */ else {
        pokracovat("Figúrka je v cieli.");
    }
} // }}}

// namiesta {{{
void namiesta() {
    for (i = 0; i < 337; i++) {
        strcpy(novepole[i], pole[i]);
    }

    for (i = 0; i < pocet; i++) {
        for (n = 0; n < 4; n++) {
            strcpy(novepole[policka[hraci[i].figurky[n].pozicia]], hraci[i].figurky[n].meno);
        }
    }

    printf("%s", resetcolor);
    system("clear");

    for (i = 0; i < 337; i++) {
        printf("%s", novepole[i]);
    }
    printf("\n\n");
} // }}}



int main() {
    init();

    pocet = cislo("Počet hráčov: ");

    // farby a mená hráčov {{{
    hraci = (Hrac *) malloc(pocet * sizeof(Hrac));
    for (i = 0; i < pocet; i++) {
        strcpy(hraci[i].farba, farby[i]);
        printf("%sMeno hráča %hd: ", hraci[i].farba, i + 1);
        fgets(etwas, 30, stdin);
        /* ClearInput(); */
        if (strlen(etwas) == 1) {
            snprintf(hraci[i].meno, 49, "%s%s %hd%s", hraci[i].farba, "hráč", i + 1, resetcolor);
        } else {
            etwas[strlen(etwas) - 1] = 0;
            snprintf(hraci[i].meno, 49, "%s%s%s", hraci[i].farba, etwas, resetcolor);
        }
        printf("\n");

        // po 4 figúrky
        for (n = 0; n < 4; n++) {
            snprintf(hraci[i].figurky[n].meno, 15, "%s%hd%s", farby[i], n + 1, resetcolor);
            // začiatočná pozícia
            hraci[i].figurky[n].pozicia = 48 + i*4 + n;
        }
    }
    // }}}

    narade = rand() % pocet;

    while ( ! hraskoncila ) {
        if ( narade == pocet - 1 ) {
            narade = 0;
        } else {
            narade += 1;
        }

        namiesta();
        printf("Na rade je %s.\n\n", hraci[narade].meno);

        tah(narade);
    }

    namiesta();
    snprintf(etwas, 49, "Vyhral/-a %s. [ENTER]", hraci[narade].meno);
    pokracovat(etwas);

    free(hraci);

    return 0;
}


// vim: foldmethod=marker
